<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Noticias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => '<div class="well well-lg text-info">{summary}</div><br>{items}<br>{pager}', //personalizamos el layout del gridView
        'summary' => 'Noticias de la <span class="label label-primary">{begin}</span> a la <span class="label label-primary">{end}</span> de un total de <span class="label label-primary">{totalCount}</span>', //personalizamos el encabezado del gridView
        'columns' => [
            'id',
            'titulo',
            'contenido:text',
            'criticas:ntext',
            [
                'attribute' => 'Criticas cortas',
                'value' => function($model) {
                    return \yii\helpers\StringHelper::truncateWords($model->criticas, 5, '...', false); //campo personalizado con texto recortado
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'options' => ["style" => "width:10%"],
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id],[
                                    'data' => [
                                        'confirm' => '¿Estas seguro que deseas eliminar la noticia?',
                                        'method' => 'post',
                                    ]
                        ]);
                    },
                ],
            ],
        ],
    ]);
    ?>
</div>
