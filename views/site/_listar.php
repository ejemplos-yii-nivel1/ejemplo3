<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>
<div class="panel-heading">
    <h2 class="panel-title"><?= Html::encode($model->titulo) ?></h2>
</div>
<div class="panel-body">
    <?= HtmlPurifier::process($model->contenido) ?>    
</div>

<div class="panel-footer color1">
    <?= Html::encode($model->criticas) ?>    
    <div class="btn-group" role="group">
    <?= Html::a("Leer mas",[
       'view1', 'id' => $model->id
    ],[
        'class' => 'btn btn-sm btn-warning'
    ])?>
    </div>
</div>
