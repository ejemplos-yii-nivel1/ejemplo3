<?php

use yii\widgets\ListView;

/* personalizamos el ListView */
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_listar',
    'itemOptions' => [
        'tag' => 'div',
        'class' => 'panel panel-primary',
    ],
    'layout' => "{pager}\n{items}\n{summary}",
    'pager' => [
        'firstPageLabel' => '<i class="glyphicon glyphicon-step-backward"></i>',
        'lastPageLabel' => '<i class="glyphicon glyphicon-step-forward"></i>',
        'nextPageLabel' => '<i class="glyphicon glyphicon-forward"></i>',
        'prevPageLabel' => '<i class="glyphicon glyphicon-backward"></i>',
        'maxButtonCount' => 4,
    ],
    'summary' => '<span class="pull-right alert bg-primary">Noticias de la {begin} a la {end} de un total de {totalCount}</span>', //personalizamos el encabezado del ListView
]);

