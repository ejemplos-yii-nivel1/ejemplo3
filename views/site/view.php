<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Noticias */

$this->title = $model->titulo;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticias-view">

    <h1><?= Html::encode($this->title) ?></h1>

     <div class="btn-group btn-group-justified" role="group">
        <?php
        if ($botones) {
            echo Html::a('Actualizar la Noticia', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
            echo Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger col-lg-offset-1',
                'data' => [
                    'confirm' => '¿Estas seguro que deseas eliminar la noticia?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
     </div>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'titulo',
            'contenido:ntext',
            'criticas:ntext',
        ],
    ])
    ?>

</div>
