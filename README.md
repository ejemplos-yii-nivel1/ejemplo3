<p align="center">
    <h1 align="center">Ejemplo numero 3 de Yii2 - Nivel 1</h1>
    <br>
</p>

<h2>Requisitos</h2>
Los requisitos minimos es que tengamos como minimo PHP 5.4

DESCRIPCION
-----------
En este ejemplo de Yii vamos a tocar los siguientes puntos:
- vamos a generar un CRUD (un sistema de Altas, Bajas y Modificaciones para una tabla de Mysql
- El nombre del controlador que vamos a utilizar es Site
- Generaremos con el Gii el controlador , el modelo y las vistas
- Vamos a personalizar el Grid-View
    - crear campos calculados
    - paginar 
    - personalizar el layout
    - personalizar los botones de accion

- Vamos a utilizar el componente ListView
    - personalizaremos el componente como el GridView

INSTALACION
------------

### INSTALANDO MEDIANTE COMPOSER

En un terminal debeis escribir lo siguiente.

~~~
composer create-project ejemplos-yii-nivel1/ejemplo3.git
~~~