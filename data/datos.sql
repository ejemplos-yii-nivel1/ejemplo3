﻿CREATE DATABASE IF NOT EXISTS ejemplo3Nivel1
	CHARACTER SET latin1
	COLLATE latin1_swedish_ci;

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

-- 
-- Set default database
--
USE ejemplo3Nivel1;

--
-- Definition for table fotos
--
CREATE TABLE IF NOT EXISTS fotos (
  id INT(11) NOT NULL AUTO_INCREMENT,
  ruta VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  noticia INT(11) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 19
AVG_ROW_LENGTH = 1170
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

--
-- Definition for table noticias
--
CREATE TABLE IF NOT EXISTS noticias (
  id INT(11) NOT NULL AUTO_INCREMENT,
  titulo VARCHAR(500) NOT NULL,
  contenido TEXT NOT NULL,
  criticas TEXT CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB
AUTO_INCREMENT = 10
AVG_ROW_LENGTH = 3276
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

-- 
-- Dumping data for table fotos
--
INSERT INTO fotos VALUES
(1, 'Animal-de-compañia-cartel-248x370.jpg', 5),
(3, 'baywatch-cartel-248x370.jpg', 3),
(5, 'Capitan-Calzoncillos-cartel-248x370.jpg', 9),
(7, 'creative-control-248x370.jpg', 1),
(8, 'Mascotas-el-mejor-estreno-de-una-pelicula-original-en-Estados-Unidos_landscape.jpg', 5),
(9, 'getImage.jpg', 5),
(10, '36369589b9e0d9c3340df0bb63c54317.jpg', 1),
(11, 'Fotos Creative Control 9.jpg', 1),
(12, 'MV5BMTEzNDQyOTkwMzheQTJeQWpwZ15BbWU4MDMwMDMxOTcx._V1_.jpg', 1),
(13, 'tumblr_inline_o3iia0V3Fv1t35chj_500.jpg', 3),
(14, '1.jpg', 3),
(15, '595460.jpg', 3),
(16, 'Poster-de-Capitan-Calzoncillos-lo-ultimo-de-DreamWorks_reference.jpg', 9),
(17, 'las-aventuras-del-capitan-c-jpg_604x0.jpg', 9),
(18, 'original.jpg', 9);

-- 
-- Dumping data for table noticias
--
INSERT INTO noticias VALUES
(1, 'Creative Control', 'En Brooklyn, un inventor crea un par de anteojos de realidad aumentada, que utiliza para engendrar un avatar de la novia de su mejor amigo. Pero su fantasía tecnológica se vuelve contra él.', 'Clásica película South By Southwest, de puro hipster, Creative Control conseguirá que te crezca la barba y te surjan gafas de pasta durante la proyección. Es de alabar su interés por enfrentarse a uno de los grandes temas de nuestro presente, el de los límites de la realidad virtual, aunque su aproximación, sin embargo, resulta algo pedestre y alejada de otras igual de discutibles pero más enriquecedoras como Black Mirror. En vez de plantearse preguntas filosóficas, director, guionistas y actores prefieren maltratar y mofarse de sus personajes y vicios. Presa de su propia trampa, acaba siendo tan hipstérica como los personajes que pretende criticar, aunque la alternancia de un soberbio blanco y negro para la vida real y los colores de la virtual la hagan resultona e interesante.'),
(3, 'Baywatch: Los vigilantes de la playa', 'Adaptación de la famosa serie de los 90. En esta versión Mitch Buchannon es Dwayne Johnson y Matt Brody, Zac Efron. Ambos socorristas, siempre enfrentados, tendrán que unirse para mantener las playas a salvo.', '¿Qué le falta y que le sobra a Baywatch para despuntar? Pues a pesar de que dura dos horas y tiene momentos puntuales en los que baja el ritmo, no se hace excesivamente larga... Como no sabes por dónde va a salir, te mantiene bien enganchado y es tan cutre que los fallos de raccord, los chroma-keys cantosos y el etalonaje del color te la refanfinfla.\r\n\r\nAl contrario de lo habitual, te hacen hasta gracia, porque lo que la peli parece pretender (y si no lo pretende, vaya chasco) es emular a un Sharknado vigorizado con el chute de adrenalina de San Andrés y aderezado con humor escatológico: vómitos, sustancias corporales varias, pechos recauchutados, abdominales venosos y vergas ad nauseam. Como si Pepe Colubi hubiera trabajado como asesor en el proceso de escritura del libreto.'),
(5, 'Animal de compañía', 'Seth está obsesionado con Holly hasta el punto que decide secuestrarla y la esconde en una jaula del refugio de animales en el que trabaja. Pero la víctima resulta no ser tan víctima...', 'Después de una prolífica trayectoria como cortometrajista y de debutar con la disfrutable Emergo, podríamos decir sin mordernos la lengua que Carles Torrens es una de las grandes promesas del panorama cinematográfico nacional.\r\n\r\nPet es una de esas películas “embotelladas” en una localización con pocos personajes. Uno de esos proyectos lowcost en los que el guión debe de ser el pilar principal.\r\n\r\nY aquí lo es. Ksenia Solo y Dominic Monaghan protagonizan un thriller escrito por Jeremy Slater (El efecto Lázaro, Cuatro fantásticos, Death Note) en el que se plantea el secuestro de una chica por parte de un hombre solitario y perturbado. Lo que parece una historia clásica y previsible se convierte en un “toma y daca” asfixiante entre los personajes gracias a un giro narrativo muy loco.\r\n\r\nPet supone para Carlos Torrens un encargo ejecutado a la perfección y lo coloca entre los candidatos a tomarle el relevo a Collet Serra en tierras norteamericanas.'),
(7, 'Señor, dame paciencia', 'La última voluntad de la mujer de Gregorio, un banquero muy tradicional, gruñón, muy madridista, es que esparcieran sus cenizas en el Guadalquivir. Para hacerlo, Gregorio tendrá que pasar el fin de semana con sus hijos y los novios de los tres: un culé, un hippie y un vasco senegalés.', 'La edad de oro que vive la comedia popular española (y que enlaza con los tiempos de los Leblanc, López Vázquez y Landa), no deja lugar para exigencias. La autoría o el riesgo pasan a un segundo plano en busca de la risa fácil (ojo, no quitemos méritos: igual que lo barato sale caro, la risa sencilla nunca lo es tanto), del jugueteo con los tópicos y arquetipos. En esta comedia, ligeramente inspirada en un bombazo francés (''Dios mío, ¿pero qué te hemos hecho?''), un pater familias tirando a facha, tiene donde fijarse: hijo gay con novio vasco... y negro, un yerno perroflauta y otro catalán. Cuadro completo. Como en nuestro cine de barrio, las ocurrencias y chistes, más o menos afortunados, quedan siempre en manos de los cómicos. Contar con Jordi Sánchez, que borda como nadie el rol de enfadón eterno desde los tiempos de ''Plats bruts'', o David Guapo siempre es un seguro de vida. El resto, dosis de buenrollismo amable y sonrisas bobaliconas.'),
(9, 'Capitán Calzoncillos: Su primer peliculón', 'Jorge y Berto son dos amigos que no saben estar quietos. ¿Las últimas de sus trastadas? Hipnotizar al director del colegio de forma que cada vez que alguien chasquea los dedos se convierte en el héroe de sus tebeos, el Capitán Calzoncillos.', 'Durante los últimos años he detectado cierta tendencia en el cine animado de gran presupuesto a buscar un mayor realismo visual. Cada cierto tiempo se destacan hitos al respecto como algo maravilloso, y no discuto que lo sea, pero sí que se trate de algo esencial para valorar el interés de la película. ¿Dónde quedaron esas filigranas que aprovechaban al máximo el hecho de no tener que respetar las leyes de la naturaleza?\r\n\r\nLo que tengo claro es que no se trata de una moda, pero sí que hay suficiente espacio para que ambas tendencias cohabiten juntas en lugar de limitarlo todo a un único modelo. ‘Capitán Calzoncillos, su primer peliculón’ (‘Captain Underpants: The First Epic Movie’) es un buen ejemplo de ello, ya que asimila los avances tecnológicos pero recupera un espíritu propio de hace varios años en los que cualquier loca parecía posible, y lo hace además de una forma bastante divertida.');

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;